package com.bartoBox.lotto;

import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ResponseBody;

@Controller
public class HomeController {

    @ResponseBody  //rezultat bedzie stringiem
    @GetMapping("/")  //po tej sciezce wywolana jest metoda hello
    public String hello() {
        return "Hello";
    }

    @ResponseBody  //rezultat bedzie stringiem
    @GetMapping("/byebye")  //po tej sciezce wywolana jest metoda hello
    public String byebye() {
        return "Nara ziomy";
    }

    @GetMapping("/welcome")
    public String welcome() {
        return "witam";  //ma zwrocic html , ktory znajduje sie w resources/templates/witam.html
    }

    @GetMapping("/about")
    public String about() {
        return "about";
    }

    @GetMapping("/lotto")
    public String generateLotto(ModelMap map) {
        LottoGenerator lottoGenerator = new LottoGenerator();
        map.put("numbers", lottoGenerator.generator());
        return "lotto";
    }






}
