package com.bartoBox.lotto;

import java.util.HashSet;
import java.util.Random;
import java.util.Set;
import java.util.TreeSet;

public class LottoGenerator {


    public Set<Integer> generator() {

        Random random = new Random();
        Set<Integer> lottoList = new TreeSet<>();
        do {
            lottoList.add(random.nextInt(49) + 1);
        } while (lottoList.size() != 6);

        return lottoList;
    }
}
